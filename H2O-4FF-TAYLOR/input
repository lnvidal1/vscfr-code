! --------------------------
! Compute the Raman spectrum
! --------------------------
DORAMAN
! --------------------------------------------------------
! Cross-sections (m2 sr-1) lower than this are not printed
! --------------------------------------------------------
.MINCS
1.D-40
! --------------------------------------------------------
! Raman shifts (cm-1) greater than this are not calculated
! --------------------------------------------------------
.MAXRSHIFT
5000.
!---------------------------
! Number of threads (OpenMP)
!---------------------------
.NTHREADS
 8
!----------------------------------------------
! Provide a script to run CFOUR
! The output must be directed to CFOUR.OUT file
!----------------------------------------------
.FPATH
/path/to/cfour/script
!-----------------------
! Maximum SCF iterations
!-----------------------
.MAXIT
50
!-------------------------
! Isotopomer specification:
!-------------------------
!.ISOTOP
! NATOM
! I1 I2 ...
!
! where:
! NATOM: number of atoms
! I1 I2 ... I_NATOM : positive integers
! I1 = 1 (most abundant isotope) 
! I1 = 2 (2nd most abundant isotope and so on)
! --------------------------------------------
.ISOTOP
3
1 1 1
!------------------------------------------
! Wavelength (nm) of the incident radiation
!------------------------------------------
.LASER
514.5
!------------------------------
! Sample's temperature (kelvin)
!------------------------------
.TEMPERATURE
400
!----------------------------
! Restart option (see README)
!----------------------------
!RESTART
!--------------------------------------------------
! Step size (bohr) to compute numerical derivatives
!--------------------------------------------------
.DQ
1.D-3
!-----------
! Title line
!-----------
.MOLECULE
H2O (1^H e 16^O)
! --------------------------------------------------------
! Number of unidimensional harmonic basis functions (VSCF)
! --------------------------------------------------------
.NBASISF
7
!------------------------------------
! RMS convergence of VSCF coeficients
! Default: 10^-6
!------------------------------------
.SCFCONV
1.D-6
! ************
! PES = Taylor
! ************
!---------------------------------
! Quadratic force constants (cm-1)
!---------------------------------
.QuadraticFC
3
    7          1650.11358
    8          3831.07277
    9          3940.68050
!-----------------------------
! Cubic force constants (cm-1)
!-----------------------------
.CubicFC
6
    7    7    7           271.3133786244
    7    7    8           310.2857598753
    7    8    8           -73.8617085926
    8    8    8         -1820.2532885307
    7    9    9          -265.7823671944
    8    9    9         -1822.7044701834
!-------------------------------
! Quartic force constants (cm-1)
!-------------------------------
.QuarticFC
15
  7    7    7    7             -50.4675466668
  8    7    7    7            -159.2151426047
  7    8    7    7            -159.2151426047
  8    8    7    7            -305.4552027139
  9    9    7    7            -368.6520726274
  7    7    8    8            -305.4941607970
  8    7    8    8              60.8821569525
  7    8    8    8              60.8821569525
  8    8    8    8             760.0051342151
  9    9    8    8             765.0727859522
  7    7    9    9            -368.7415070804
  8    7    9    9             118.3489562268
  7    8    9    9             118.3489562266
  8    8    9    9             765.0165369014
  9    9    9    9             771.6990382423
!-------------------
! Delta v_max option 
!-------------------
.DVMAX
0
!-----------------------------------------
! CI expansion: 
! VCS/ VCISD / VCISDT / VCI4 / VCI5 / VCI6
!-----------------------------------------
VCISDT
!--------------------------------------------------------
! Uncomment to remove Hc from the vibrational Hamiltonian
! Note: Hc is not included in VSCF calculations;
! thus, this option affects only VCI calculations
!--------------------------------------------------------
!NOWATSON
