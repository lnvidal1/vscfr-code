c Maximum number of lines read in ZMAT
c Maximum number of atoms
      PARAMETER (MAXLIN=100, MAXAT=5)

c Maximum number of basis functions (VSCF)
      PARAMETER (MAXB=20)

c Maximum number of VCI configurations
      PARAMETER (MAXSTATES=25000)

c Maximum number of points in the Gauss-Hermite quadrature
      PARAMETER (MAXNP=20)
